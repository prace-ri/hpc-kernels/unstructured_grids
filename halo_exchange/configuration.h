#ifndef CONFIGURATION_H
#define CONFIGURATION_H

#include <stdio.h>

#include "box.h"

enum verbosity_level_enum
{
   OFF = 0,
   INFO = 1,
   DEBUG = 2,
   TRACE = 3
};

enum transfer_mode_enum
{
   SPARSE_COLLECTIVE = 0,
   COLLECTIVE = 1,
   P2P_DEFAULT = 2,
   P2P_SYNCHRONOUS = 3,
   P2P_READY = 4
};

typedef struct
{
   int verbosity_level;
   int debug_rank;

   int halo_width;
   int transfer_mode;
   int n_iterations;

   box_t global_domain;
} conf_t;

// Meta-information for MPI-Datatype creation (see mpitypes.c)
#define CONF_T_N_INT_MEMBERS 5
#define CONF_T_FIRST_INT_MEMBER verbosity_level
#define CONF_T_N_BOX_MEMBERS 1
#define CONF_T_FIRST_BOX_MEMBER global_domain

void conf_init(conf_t *c);

const char* conf_set_from_args(conf_t *c, int argc, char* argv[]);

void conf_print(const conf_t *c, FILE *file);

int info_enabled(const conf_t *c);
int warn_enabled(const conf_t *c);
int debug_enabled(const conf_t *c);
int trace_enabled(const conf_t *c);

#endif

